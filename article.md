title: "Sněmovna jako žvanírna? Babiš je v desítce nejvýřečnějších poslanců. Žebříček vede Stanjura"
perex: "Spočítali jsme, kolik času tráví poslanci u řečnického pultu. Rozdíly jsou propastné."
authors: ["Michal Zlatkovský"]
coverimg: https://www.irozhlas.cz/sites/default/files/images/03527521.jpeg
coverimg_note: ""
styles: ["//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css", "https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css"]
libraries: ["https://unpkg.com/jquery@3.2.1", "//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js", "https://code.highcharts.com/highcharts.js", "https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"]
---

Zatímco každý z první desítky v žebříčku poslanců si v aktuálním funkčním období může na konto připsat přes 200 tisíc slov, celkem sedmnáct zákonodárců na plénu v posledních čtyřech letech nenamluvilo ani čtvrt hodiny.

Práce poslance se sice neměří na prodiskutované hodiny v jednacím sále, přesto čas strávený za řečnickým pultem do jisté míry ukazuje, jak je zákonodárce aktivní. Pro připomenutí: při posuzování aktivity poslance ve sněmovně je třeba počítat i s jeho prací ve výborech, komisích a klubech či jeho zapojení do tvorby nových a úpravy starých zákonů.

Rekordmanem v řečnění je s přehledem Zbyněk Stanjura z opoziční ODS. V Poslanecké sněmovně pronesl přes 662 tisíc slov, o polovinu víc, než předseda vlády Bohuslav Sobotka. V nejvýřečnější desítce se kromě sněmovních předsedajících umístil i místopředseda vlády Andrej Babiš. Ten přitom dlouhé diskuze na půdě sněmovny opakovaně kritizuje a označuje ji za žvanírnu.
 
Jedním z nejviditelnějších řečníků je i předseda TOP 09 Miroslav Kalousek, který se umístil na čtvrtém místě. Ten ovšem smysl žebříčku důrazně popírá. "Je to naprosto nehorázné měření," řekl pro iROZHLAS.cz. "Příště rovnou pošleme do sněmovny osla, ten může hýkat od rána do večera a bude vítěz, protože toho nahýká nejvíc."<br><br>

<table id="poslanci" width="100%" class="display responsive"></table>
<i>Tabulka obsahuje data pro všechny současné i bývalé poslance v aktuálním volebním období a ministry vlády Bohuslava Sobotky k 24. dubnu 2017. Poslanci jsou započítáni vždy do strany, která je nominovala na svou kandidátní listinu. Výpočet ze [sněmovních stenoprotokolů](http://www.psp.cz/eknih/2013ps/stenprot/index.htm), odhad času podle průměrné rychlosti mluvení 11 znaků za sekundu.</i><br><br>

Ne každý poslanec toho ovšem namluví zdaleka tolik jako sněmovní rekordmani. Na opačném konci žebříčku je celkem dvanáct poslanců, kteří ve svém funkčním období nepronesli před sněmovním mikrofonem ani tisíc slov. Celkem tak u řečnického pultíku strávili méně než deset minut. Jeden z nich, Lubomír Toufar z vládní ČSSD, to přičítá špatnému jednacímu řádu. "Ve sněmovně je to rozdělené na lepší a horší lidi - s přednostním právem mluvit a bez něj. I ty nejmenší strany také můžou zdržovat projednávání zákonů." Kritizuje i to, že zkušenější poslanci nechtějí na schůzích dávat prostor nováčkům. "Sám jsem ve dvou výborech a tam je úroveň úplně jiná, věci se projednávají slušně. Jakmile ale přijdou kamery na plénum, někteří se hned hlasí, jen aby byli v televizi. To nepotřebuju a nechci."

Podobný postoj má i nezařazený poslanec původně zvolený za Úsvit Karel Pražák, který na plénu s výjimkou složení slibu dokonce nemluvil nikdy. "Nemám za sebou žádný klub, svůj názor dávám najevo hlasováním. Abych vystupoval sám bez jakékoliv podpory je zbytečné." K návrhům zákonů se vyjadřuje ve sněmovních výborech a sněmovní schůze nepovažuje za vhodný prostor pro vyjadřování názorů. "Sněmovna je dnes továrna na zákony," řekl serveru iROZHLAS.cz.

Průměrně, v přepočtu slov na poslance, vede i díky Zbyňku Stanjurovi s výrazným náskokem ODS. Ta má s 116tisícovým průměrem dvojnásobný náskok oproti druhé TOP 09. Nejméně oproti tomu mluví průměrný poslanec ANO: kolem 35 tisíc slov, oproti ODS méně než třetinu.

<div id="strany"></div>

Ženy se sněmovních debat účastní méně, než by odpovídalo jejich zastoupení. Zatímco z poslanců tvoří ženy pětinu, u mikrofonu stráví jen asi 14 procent celkového času. S výjimkou místopředsedkyně sněmovny Jaroslavy Pokorné Jermanové je první žena v žebříčku Jana Černochová z ODS na patnáctém místě.

<div id="muziZeny"></div>

