
# coding: utf-8

# In[18]:


import os, re
from lxml import html


# In[40]:


def merge_content(folder):
    filelist = []

    for root, dirs, files in os.walk(folder):
        for filename in files:
            if filename[0] == 's':
                filelist.append(root+"/"+filename)

    filelist.sort()
    content = []
    
    for file in filelist:
        with open(file, 'r') as file:
            start = 0
            for line in file:
                if "<!-- eh -->" in line or "<!-- ex -->" in line: start = 1
                if "<!-- sf -->" in line or "<!-- sy -->" in line: start = 0
                if start == 1: content.append(line)
    
    return(content)


# In[142]:


def merged_html_out(folder,filename):
    content = merge_content(folder)
    with open(filename, 'w') as output:
        output.write('<html>\n<head>\n<title>'+folder+'</title>\n</head>\n<body>\n')
        for line in content: output.write(line)
        output.write('</body>\n</html>')


# In[143]:


def merged_xml_out(folder,filename):
    content = merge_content(folder)
    output = []
    speaking = ""
    for line in content:
        line = line.replace('&nbsp;',' ')
        line = line.replace('***','')
        line = re.sub(' ?\(.*?\)',"", line)
        if "<b><a href=" in line:
            if speaking: output.append("</speech>\n\n")
            speaking = re.split('[<>]',line)
            output.append("<speech speaker=\""+speaking[6]+"\">"+html.document_fromstring(line).text_content()[len(speaking[6])+1:].strip())
        elif "<a href=" in line[:30] and "hlasy.sqw" not in line and "historie.sqw" not in line:
            if speaking: output.append("</speech>\n\n")
            speaking = re.split('[<>]',line)
            output.append("<speech speaker=\""+speaking[4]+"\">"+speaking[6][1:].strip())
        elif "<b><u>" in line:
            if speaking: output.append("</speech>\n\n")
            speaking = re.split('[<>]',line)
            output.append("<speech speaker=\""+speaking[6]+"\">"+speaking[10][1:].strip())           
        elif speaking:
            try: line = html.document_fromstring(line).text_content()
            except:
                line += "<div></div>"
                line = html.document_fromstring(line).text_content()
            if line.strip() != "": output.append(" " + line.strip())
    output.append("</speech>")

    with open(filename, 'w') as outfile:
        for line in output: outfile.write(line)


# In[144]:


os.makedirs("html", exist_ok=True)
os.makedirs("xml", exist_ok=True)

folderlist = []
for i in range(1,62): folderlist.append(str(i).zfill(3))
for folder in folderlist:
    merged_html_out('./data/'+folder+'schuz','html/merged'+folder+'.html')
    merged_xml_out('./data/'+folder+'schuz','xml/merged'+folder+'.xml')


# In[145]:


filelist = []

for root, dirs, files in os.walk('./xml'):
    for filename in files: filelist.append(root+"/"+filename)

filelist.sort()

spklen = {}

for file in filelist:
    with open(file, 'r') as file:
        singleline = ""
        for line in file: singleline += line
        tree = html.fromstring(singleline)
        speakers = tree.xpath("//speech/@speaker")
        speeches = tree.xpath("//speech/text()")
        for i,x in enumerate(speakers):
            if speakers[i] not in spklen: 
                spklen[speakers[i]] = [len(speeches[i]),len(speeches[i].split())]
            else:
                spklen[speakers[i]][0] += len(speeches[i])
                spklen[speakers[i]][1] += len(speeches[i].split())            

with open('stats.csv', 'w') as outfile:
    outfile.write('jmeno;znaku;slov\n')
    for line in spklen:
        out = line + ";" + str(spklen[line][0]) + ";" + str(spklen[line][1]) + "\n"
        outfile.write(out)


# In[146]:


stats = []
for entry,count in spklen.items():
    possible_roles = ["Poslanec","Poslankyně", "Senátorka", "Senátor", "Místopředseda PSP", "Místopředsedkyně PSP",
                      "Předseda PSP", "Předsedající", "Člen zastupitelstva hl. m. Prahy", "Zástupce veřejného ochránce práv",
                      "Veřejná ochránkyně práv", "Předseda vlády ČR", "Primátor hl. m. Prahy", "Prezident České republiky",
                      "Prezident Nejvyššího kontrolního úřadu", "Místopředseda vlády a ministr financí ČR",
                      "Místopředseda vlády ČR a ministr financí", "Místopředseda vlády ČR", 
                      "Starosta Vlachovic - Vrbětic", "Generální ředitel České pošty", "Guvernér ČNB",
                      "Náměstek hejtmana Zlínského kraje", "Náměstek hejtmana Moravskoslezského kraje",
                      "Evropská komisařka pro spravedlnost, spotřebitele a rovnost žen a mužů",
                      "Hejtman Libereckého kraje", "Hejtman Středočeského kraje","Paní","Pan" 
                     ]
    namefix = {"Zuzka Bebarová Rujbrová": "Zuzka Bebarová-Rujbrová",
               "Hana Aulická Jírovcová": "Hana Aulická-Jírovcová",
               "Jaroslava Jermanová": "Jaroslava Pokorná Jermanová",
               "Markéta Adamová": "Markéta Pekarová Adamová"
              }
    role = ""
    for possible_role in possible_roles:
        if possible_role in entry: role = possible_role
        if re.search('Ministr.*ČR', entry):
            role = re.search('Ministr.*ČR', entry).group(0)
            entry = re.sub('Ministr.*ČR',"", entry)
        entry = entry.replace(possible_role,"").strip()
    for name,fixedname in namefix.items():
        if name in entry:
            entry = entry.replace(name,fixedname)
    stats.append([entry,role,count[0],count[1]])

stats.sort()

with open('stats.csv', 'w') as outfile:
    outfile.write('jmeno;role;znaků;slov\n')
    for stat in stats:
        out = stat[0] + ";" + stat[1] + ";" + str(stat[2]) + ";" + str(stat[3]) + "\n"
        outfile.write(out)


# In[12]:


import csv
with open('stats_clean.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    malewords = 0
    femalewords = 0
    strany = {}
    for row in csvreader:
        if row [3] == "M": malewords += int(row[6])
        if row [3] == "F": femalewords += int(row[6])
        if row [6] != "slov":
            if row [4] not in strany:
                strany[row[4]]=int(row[6])
            else:
                strany[row[4]]+=int(row[6])
    print(malewords, femalewords)
    for a,b in strany.items():
        print(a,b)

