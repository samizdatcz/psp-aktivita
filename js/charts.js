Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('muziZeny', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Projevy ve sněmovně: muži vs. ženy'
        },
       subtitle: {
          text: 'V Poslanecké sněmovně je v současnosti 20 % žen (40 z 200 poslanců).'
        },
        credits: {
            enabled: false
        },          
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f} %</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Podíl ze všech projevů (v počtu slov)',
            colorByPoint: true,
            data: [{
                name: 'Muži',
                y: 86.06
            }, {
                name: 'Ženy',
                y: 13.94,
                sliced: true,
                selected: true
            }]
        }]
    });

Highcharts.chart('strany', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Projevy ve sněmovně podle stran'
    },
    subtitle: {
        text: 'Přepočteno na jednoho poslance'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ČSSD',
                    'ANO',
                    'KDU-ČSL',
                    'KSČM',     
                    'TOP 09',                                   
                    'ODS',
                    'Úsvit'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Počet slov na poslance:'
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.05,
            borderWidth: 0
        }
    },
    legend: {
            enabled: false
    },
    series: [{
        name: 'Počet slov na poslance',
        data: [39390, 34833, 54889, 38482, 66353, 116521, 41504]

    }]
});