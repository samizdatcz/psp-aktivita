  var data = [
  ["Zbyněk","Stanjura","Poslanec","M","ODS","3990936","662080","5543"  ],
  ["Vojtěch","Filip","Místopředseda PSP","M","KSČM","3378914","521398","4693"  ],
  ["Bohuslav","Sobotka","Předseda vlády","M","ČSSD","2555308","395532","3549"  ],
  ["Miroslav","Kalousek","Poslanec","M","TOP 09","2393217","381569","3324"  ],
  ["Petr","Gazdík","Místopředseda PSP","M","TOP 09","2490049","376149","3458"  ],
  ["Jan","Bartošek","Místopředseda PSP","M","KDU-ČSL","2040096","320868","2833"  ],
  ["Jan","Hamáček","Předseda PSP","M","ČSSD","1992902","315031","2768"  ],
  ["František","Laudát","Poslanec","M","TOP 09","1548619","249417","2151"  ],
  ["Andrej","Babiš","Místopředseda vlády,<br>ministr financí, poslanec","M","ANO","1399335","220956","1944"  ],
  ["Jaroslava","Pokorná Jermanová","Místopředsedkyně PSP (2013-17)","F","ANO","1429370","219908","1985"  ],
  ["Petr","Bendl","Poslanec","M","ODS","1240969","202193","1724"  ],
  ["Tomio","Okamura","Poslanec","M","Úsvit","956626","153544","1329"  ],
  ["Lubomír","Zaorálek","Ministr zahraničí,<br>poslanec","M","ČSSD","806714","133251","1120"  ],
  ["Petr","Fiala","Poslanec","M","ODS","832408","133009","1156"  ],
  ["Ludvík","Hovorka","Poslanec","M","KDU-ČSL","840575","127363","1167"  ],
  ["Jana","Černochová","Poslankyně","F","ODS","793129","126415","1102"  ],
  ["Jan","Zahradník","Poslanec","M","ODS","798330","126268","1109"  ],
  ["Karel","Fiedler","Poslanec","M","Úsvit","773122","124913","1074"  ],
  ["Milan","Chovanec","Ministr vnitra","M","ČSSD","728336","113726","1012"  ],
  ["Richard","Brabec","Ministr životního prostředí,<br>poslanec","M","ANO","702711","110174","976"  ],
  ["Michal","Kučera","Poslanec","M","TOP 09","687726","108871","955"  ],
  ["Marek","Benda","Poslanec","M","ODS","655392","105896","910"  ],
  ["Martin","Kolovratník","Poslanec","M","ANO","646843","102568","898"  ],
  ["Vladislav","Vilímec","Poslanec (2014-)","M","ODS","644832","100461","896"  ],
  ["Pavel","Kováčik","Poslanec","M","KSČM","577099","92848","802"  ],
  ["Jan","Klán","Poslanec","M","KSČM","572172","92367","795"  ],
  ["Marian","Jurečka","Ministr zemědělství,<br>poslanec","M","KDU-ČSL","583089","91009","810"  ],
  ["Miroslava","Němcová","Poslankyně","F","ODS","518614","82763","720"  ],
  ["Václav","Votava","Poslanec","M","ČSSD","517893","81084","719"  ],
  ["Marek","Černoch","Poslanec","M","Úsvit","448830","72787","623"  ],
  ["Zdeněk","Ondráček","Poslanec","M","KSČM","451636","72118","627"  ],
  ["Helena","Válková","Ministryně spravedlnosti (2014-15),<br>poslankyně","F","ANO","458581","70674","637"  ],
  ["Jitka","Chalánková","Poslankyně","F","TOP 09","451851","70471","628"  ],
  ["Radek","Vondráček","Místopředseda PSP (2017-),<br>poslanec","M","ANO","433438","68487","602"  ],
  ["Dan","Ťok","Ministr dopravy","M","ANO","443199","68343","616"  ],
  ["Jan","Mládek","Ministr průmyslu (2014-17),<br>poslanec","M","ČSSD","432608","65306","601"  ],
  ["Věra","Kovářová","Poslankyně","F","TOP 09","418831","64427","582"  ],
  ["Marta","Semelová","Poslankyně","F","KDU-ČSL","383880","59753","533"  ],
  ["Martin","Stropnický","Ministr obrany, poslanec","M","ANO","381561","59414","530"  ],
  ["Leoš","Heger","Poslanec","M","TOP 09","379076","58906","526"  ],
  ["Ivan","Adamec","Poslanec","M","ODS","338561","56383","470"  ],
  ["Karla","Šlechtová","Ministryně pro místní rozvoj","F","ANO","365986","56374","508"  ],
  ["Daniel","Herman","Ministr kultury, poslanec","M","KDU-ČSL","369702","55289","513"  ],
  ["Jiří","Dolejš","Poslanec","M","KSČM","339539","55042","472"  ],
  ["Svatopluk","Němeček","Ministr zdravotnictví (2014-16)","M","ČSSD","362808","53929","504"  ],
  ["Jeroným","Tejc","Poslanec","M","ČSSD","328482","53333","456"  ],
  ["Antonín","Seďa","Poslanec","M","ČSSD","348895","52607","485"  ],
  ["Jana","Hnyková","Poslankyně","F","Úsvit","327594","51332","455"  ],
  ["Michaela","Marksová","Ministryně práce a sociálních věcí","F","ČSSD","316689","50770","440"  ],
  ["Leo","Luzar","Poslanec (2014-)","M","KSČM","315430","50426","438"  ],
  ["Miroslav","Opálka","Poslanec","M","KSČM","307735","49222","427"  ],
  ["Jaroslav","Faltýnek","Poslanec","M","ANO","294988","47374","410"  ],
  ["Kateřina","Valachová","Ministryně školství","F","ČSSD","303260","45754","421"  ],
  ["Jiří","Dienstbier","Ministr pro lidská práva (2014-16)","M","ČSSD","290195","44618","403"  ],
  ["Ivan","Pilný","Poslanec","M","ANO","280382","44473","389"  ],
  ["Jiří","Mihola","Poslanec","M","KDU-ČSL","280369","44356","389"  ],
  ["Jiří","Zlatuška","Poslanec","M","ANO","284801","43963","396"  ],
  ["Pavel","Blažek","Poslanec","M","ODS","256644","42512","356"  ],
  ["Martin","Plíšek","Poslanec (2014-)","M","TOP 09","277395","42405","385"  ],
  ["Bohuslav","Svoboda","Poslanec","M","ODS","251382","41052","349"  ],
  ["Adolf","Beznoska","Poslanec (2013-17)","M","ODS","259550","41007","360"  ],
  ["Miroslav","Grebeníček","Poslanec","M","KSČM","270311","40781","375"  ],
  ["Jan","Chvojka","Ministr pro lidská práva,<br>poslanec","M","ČSSD","251076","40692","349"  ],
  ["Ladislav","Šincl","Poslanec","M","ČSSD","250299","38532","348"  ],
  ["Jan","Farský","Poslanec","M","TOP 09","237784","38508","330"  ],
  ["Robert","Pelikán","Ministr spravedlnosti","M","ANO","239132","38425","332"  ],
  ["Martin","Novotný","Poslanec","M","ODS","243797","38053","339"  ],
  ["Roman","Sklenák","Poslanec","M","ČSSD","235363","37813","327"  ],
  ["Jana","Fischerová","Poslankyně","F","ODS","232182","37607","322"  ],
  ["Martin","Lank","Poslanec","M","Úsvit","230219","36326","320"  ],
  ["Milan","Urban","Poslanec","M","ČSSD","225034","36210","313"  ],
  ["Markéta","Pekarová Adamová","Poslankyně","F","TOP 09","227242","36030","316"  ],
  ["Soňa","Marková","Poslankyně","F","KSČM","234209","35468","325"  ],
  ["Ivan","Gabal","Poslanec","M","KDU-ČSL","226251","35021","314"  ],
  ["Herbert","Pavera","Poslanec","M","TOP 09","210207","33993","292"  ],
  ["Olga","Havlová","Poslankyně","F","Úsvit","216231","33664","300"  ],
  ["Daniel","Korte","Poslanec","M","TOP 09","211541","32445","294"  ],
  ["Nina","Nováková","Poslankyně","F","TOP 09","200241","32263","278"  ],
  ["Antonín","Prachař","Ministr dopravy (2014)","M","ANO","215170","32055","299"  ],
  ["Jiří","Štětina","Poslanec","M","Úsvit","194030","31397","269"  ],
  ["Stanislav","Grospič","Poslanec","M","KSČM","200687","31159","279"  ],
  ["Simeon","Karamazov","Poslanec","M","ODS","203667","30770","283"  ],
  ["Anna","Putnová","Poslankyně","F","TOP 09","197398","30426","274"  ],
  ["František","Vácha","Poslanec","M","TOP 09","181614","29066","252"  ],
  ["Marcel","Chládek","Ministr školství (2014-15)","M","ČSSD","181162","29059","252"  ],
  ["Pavel","Bělobrádek","Místopředseda vlády,<br>místopředseda PSP (2013-14),<br>poslanec","M","KDU-ČSL","170510","27637","237"  ],
  ["Jiří","Valenta","Poslanec","M","KSČM","183340","27604","255"  ],
  ["Jaroslav","Zavadil","Poslanec","M","ČSSD","165027","27548","229"  ],
  ["Bohuslav","Chalupa","Poslanec","M","ANO","170132","27267","236"  ],
  ["Rostislav","Vyzula","Poslanec","M","ANO","169981","27173","236"  ],
  ["Karel","Šidlo","Poslanec","M","KSČM","173317","26612","241"  ],
  ["Hana","Aulická-Jírovcová","Poslankyně","F","KSČM","168531","26233","234"  ],
  ["Vít","Kaňkovský","Poslanec","M","KDU-ČSL","172300","26117","239"  ],
  ["Radim","Fiala","Poslanec","M","Úsvit","164669","25916","229"  ],
  ["Radim","Holeček","Poslanec","M","ODS","166822","25891","232"  ],
  ["Václav","Zemek","Poslanec","M","ČSSD","157148","25230","218"  ],
  ["Josef","Hájek","Poslanec","M","ANO","151925","24500","211"  ],
  ["Radka","Maxová","Poslankyně","F","ANO","160606","24456","223"  ],
  ["Stanislav","Polčák","Poslanec (2013-14)","M","TOP 09","149772","23819","208"  ],
  ["Jiří","Koubek","Poslanec","M","TOP 09","148382","23764","206"  ],
  ["Karel","Rais","Poslanec","M","ANO","153274","23724","213"  ],
  ["Václav","Klučka","Poslanec","M","ČSSD","153183","23601","213"  ],
  ["Jan","Volný","Poslanec","M","ANO","141412","23067","196"  ],
  ["Pavel","Plzák","Poslanec","M","ANO","142570","23063","198"  ],
  ["Martin","Komárek","Poslanec","M","ANO","140472","22802","195"  ],
  ["Bronislav","Schwarz","Poslanec","M","ANO","131377","22195","182"  ],
  ["Jaroslav","Klaška","Poslanec","M","KDU-ČSL","145486","22064","202"  ],
  ["Petr","Kudela","Poslanec","M","KDU-ČSL","143728","21919","200"  ],
  ["Zdeněk","Soukup","Poslanec","M","ANO","136363","21216","189"  ],
  ["Robin","Böhnisch","Poslanec","M","ČSSD","133393","20444","185"  ],
  ["Petr","Kořenek","Poslanec","M","ČSSD","119984","18735","167"  ],
  ["Miloslav","Ludvík","Ministr zdravotnictví","M","ČSSD","118138","18719","164"  ],
  ["Štěpán","Stupčuk","Poslanec","M","ČSSD","113918","17844","158"  ],
  ["Jiří","Koskuba","Poslanec","M","ČSSD","104078","17646","145"  ],
  ["Vladimír","Koníček","Poslanec","M","KSČM","110458","17077","153"  ],
  ["Ivana","Dobešová","Poslankyně","F","ANO","107507","16910","149"  ],
  ["Helena","Langšádlová","Poslankyně","F","TOP 09","102327","16126","142"  ],
  ["Ondřej","Benešík","Poslanec","M","KDU-ČSL","101296","16065","141"  ],
  ["Lukáš","Pleticha","Poslanec","M","ČSSD","100647","16018","140"  ],
  ["Jan","Birke","Poslanec","M","ČSSD","93875","15011","130"  ],
  ["Jana","Lorencová","Poslankyně","F","ANO","91246","14840","127"  ],
  ["Rom","Kostřica","Poslanec","M","TOP 09","92766","14687","129"  ],
  ["Jiří","Skalický","Poslanec","M","TOP 09","93316","14448","130"  ],
  ["Jaroslav","Foldyna","Poslanec","M","ČSSD","88974","14195","124"  ],
  ["Jana","Pastuchová","Poslankyně","F","ANO","90782","14097","126"  ],
  ["Jiří","Junek","Poslanec","M","KDU-ČSL","84431","13965","117"  ],
  ["Jaroslav","Krákora","Poslanec","M","ČSSD","88463","13959","123"  ],
  ["Alena","Nohavová","Poslankyně","F","KSČM","89577","13652","124"  ],
  ["Vlasta","Bohdalová","Poslankyně (2014-)","F","ČSSD","84478","13476","117"  ],
  ["František","Adámek","Poslanec","M","ČSSD","85656","13442","119"  ],
  ["Jiří","Petrů","Poslanec","M","ČSSD","88456","13322","123"  ],
  ["Kateřina","Konečná","Poslankyně (2013-14)","F","KSČM","84266","13275","117"  ],
  ["Matěj","Fichtner","Poslanec","M","ANO","84364","13078","117"  ],
  ["David","Kasal","Poslanec","M","ANO","82257","13075","114"  ],
  ["Karel","Schwarzenberg","Poslanec","M","TOP 09","83341","13062","116"  ],
  ["Kristýna","Zelienková","Poslankyně","F","ANO","81480","12597","113"  ],
  ["Gabriela","Pecková","Poslankyně","F","TOP 09","80831","12425","112"  ],
  ["Věra","Jourová","Min. pro místní rozvoj (2014),<br>poslankyně (2013-14)","F","ANO","77316","12362","107"  ],
  ["Josef","Nekl","Poslanec","M","KSČM","78785","12247","109"  ],
  ["Petr","Adam","Poslanec","M","Úsvit","79172","12160","110"  ],
  ["Igor","Nykl","Poslanec","M","ANO","73504","12083","102"  ],
  ["David","Kádner","Poslanec","M","Úsvit","72699","11567","101"  ],
  ["Jaroslav","Holík","Poslanec","M","Úsvit","71259","11409","99"  ],
  ["Roman","Váňa","Poslanec","M","ČSSD","73521","11408","102"  ],
  ["René","Číp","Poslanec","M","KSČM","73956","11233","103"  ],
  ["Alexander","Černý","Poslanec","M","KSČM","67350","10788","94"  ],
  ["Roman","Procházka","Poslanec","M","ANO","70812","10667","98"  ],
  ["Jiří","Pospíšil","Poslanec (2013-14)","M","ODS","64979","10366","90"  ],
  ["Marie","Pěnčíková","Poslankyně","F","KSČM","61732","10003","86"  ],
  ["Vlastimil","Vozka","Poslanec","M","ANO","65159","9865","90"  ],
  ["Pavel","Šrámek","Poslanec","M","ANO","61296","9605","85"  ],
  ["Václav","Horáček","Poslanec","M","TOP 09","60212","9577","84"  ],
  ["Roman","Kubíček","Poslanec","M","ANO","62746","9424","87"  ],
  ["Josef","Novotný","Poslanec","M","ČSSD","59076","9386","82"  ],
  ["Václav","Snopek","Poslanec","M","KSČM","60872","9334","85"  ],
  ["Pavlína","Nytrová","Poslankyně","F","ČSSD","60216","9154","84"  ],
  ["Igor","Jakubčík","Poslanec","M","ČSSD","57164","9016","79"  ],
  ["Dana","Váhalová","Poslankyně","F","ČSSD","60022","8904","83"  ],
  ["Milan","Šarapatka","Poslanec","M","Úsvit","54550","8561","76"  ],
  ["Josef","Uhlík","Poslanec","M","KDU-ČSL","55226","8332","77"  ],
  ["Miloslav","Janulík","Poslanec","M","ANO","48267","8009","67"  ],
  ["Margita","Balaštíková","Poslankyně","F","ANO","49787","7909","69"  ],
  ["Zuzka","Bebarová-Rujbrová","Poslankyně","F","KSČM","51531","7878","72"  ],
  ["Marek","Ženíšek","Poslanec","M","TOP 09","48116","7699","67"  ],
  ["Stanislav","Mackovík","Poslanec","M","KSČM","48490","7696","67"  ],
  ["Augustin Karel","Andrle Sylor","Poslanec","M","Úsvit","47843","7481","66"  ],
  ["Stanislav","Huml","Poslanec","M","ČSSD","43964","7281","61"  ],
  ["Martina","Berdychová","Poslankyně","F","ANO","47755","7024","66"  ],
  ["Ladislav","Velebný","Poslanec","M","ČSSD","47167","7022","66"  ],
  ["Vítězslav","Jandák","Poslanec","M","ČSSD","35373","6036","49"  ],
  ["Josef","Zahradníček","Poslanec","M","KSČM","38895","6021","54"  ],
  ["Josef","Kott","Poslanec","M","ANO","39302","5807","55"  ],
  ["Zuzana","Šánová","Poslankyně (2014-)","F","ANO","36533","5705","51"  ],
  ["Květa","Matušovská","Poslankyně","F","KSČM","38856","5612","54"  ],
  ["Marie","Benešová","Poslankyně","F","ČSSD","35748","5607","50"  ],
  ["Zdeněk","Syblík","Poslanec (2014-)","M","ČSSD","35781","5335","50"  ],
  ["Jiří","Holeček","Poslanec","M","ANO","31994","4890","44"  ],
  ["Ladislav","Okleštěk","Poslanec","M","ANO","30663","4851","43"  ],
  ["Pavla","Golasowská","Poslankyně (2014-)","F","KDU-ČSL","31767","4839","44"  ],
  ["Josef","Šenfeld","Poslanec","M","KSČM","30222","4549","42"  ],
  ["Jaroslav","Borka","Poslanec","M","KSČM","30413","4501","42"  ],
  ["Jiří","Běhounek","Poslanec","M","ČSSD","28064","4328","39"  ],
  ["Milan","Brázdil","Poslanec","M","ANO","24757","4112","34"  ],
  ["Milada","Halíková","Poslankyně","F","KSČM","26812","4080","37"  ],
  ["Tomáš Jan","Podivínský","Poslanec (2013-14)","M","KDU-ČSL","23663","3628","33"  ],
  ["Zuzana","Kailová","Poslankyně","F","ČSSD","22237","3381","31"  ],
  ["Gabriela","Hubáčková","Poslankyně","F","KSČM","22221","3379","31"  ],
  ["Stanislav","Berkovec","Poslanec","M","ANO","22133","3349","31"  ],
  ["Zdeněk","Bezecný","Poslanec","M","TOP 09","22061","3171","31"  ],
  ["Ivo","Pojezný","Poslanec","M","KSČM","19220","3000","27"  ],
  ["Pavel","Holík","Poslanec","M","ČSSD","21097","2963","29"  ],
  ["Pavel","Čihák","Poslanec","M","ANO","18879","2925","26"  ],
  ["Richard","Dolejš","Poslanec","M","ČSSD","19208","2892","27"  ],
  ["Miloš","Babiš","Poslanec","M","ANO","18637","2796","26"  ],
  ["Josef","Vozdecký","Poslanec (2013-16)","M","ANO","18730","2788","26"  ],
  ["Adam","Rykala","Poslanec","M","ČSSD","16115","2523","22"  ],
  ["Pavel","Antonín","Poslanec","M","ČSSD","16218","2514","23"  ],
  ["Miloslava","Vostrá","Poslankyně","F","KSČM","15881","2396","22"  ],
  ["Jiří","Havlíček","Ministr průmyslu","M","ČSSD","15488","2282","22"  ],
  ["Jiří","Zemánek","Poslanec","M","ČSSD","14692","2202","20"  ],
  ["Martin","Sedlář","Poslanec","M","ANO","13807","2037","19"  ],
  ["Karel","Černý","Poslanec","M","ČSSD","12974","1938","18"  ],
  ["Markéta","Wernerová","Poslankyně","F","ČSSD","11107","1662","15"  ],
  ["Pavel","Havíř","Poslanec","M","ČSSD","11040","1645","15"  ],
  ["Michal","Hašek","Poslanec (2013-14)","M","ČSSD","10497","1628","15"  ],
  ["Jan","Skopeček","Poslanec (2017-)","M","ODS","9904","1620","14"  ],
  ["Stanislav","Pfléger","Poslanec","M","ANO","9606","1395","13"  ],
  ["Vojtěch","Adam","Poslanec","M","KSČM","8378","1275","12"  ],
  ["Jaroslav","Lobkowicz","Poslanec","M","TOP 09","6807","1180","9"  ],
  ["Jiří","Zimola","Poslanec (2013-14)","M","ČSSD","7373","1101","10"  ],
  ["Miroslava","Strnadlová","Poslankyně","F","ČSSD","5810","917","8"  ],
  ["Miloslava","Rutová","Poslankyně (2016-)","F","ANO","5235","858","7"  ],
  ["Miloš","Petera","Poslanec (2013-14)","M","ČSSD","4936","846","7"  ],
  ["Pavel","Ploc","Poslanec","M","ČSSD","5075","779","7"  ],
  ["Josef","Vondrášek","Poslanec","M","KSČM","3906","644","5"  ],
  ["František","Petrtýl","Poslanec (2017-)","M","ANO","3130","499","4"  ],
  ["Pavel","Volčík","Poslanec","M","ANO","3283","494","5"  ],
  ["Jan","Sedláček","Poslanec","M","ANO","3034","447","4"  ],
  ["Karel","Tureček","Poslanec","M","TOP 09","1865","264","3"  ],
  ["Lubomír","Toufar","Poslanec","M","ČSSD","1059","164","1"  ],
  ["Vlastimil","Gabrhel","Poslanec (2014-)","M","ČSSD","1070","154","1"  ],
  ["Karel","Pražák","Poslanec","M","Úsvit","0","0","0"  ]
];


$.fn.dataTable.ext.order.intl = function ( locales, options ) {
    if ( window.Intl ) {
        var collator = new window.Intl.Collator( locales, options );
        var types = $.fn.dataTable.ext.type;
 
        delete types.order['string-pre'];
        types.order['string-asc'] = collator.compare;
        types.order['string-desc'] = function ( a, b ) {
            return collator.compare( a, b ) * -1;
        };
    }
};

$(document).ready(function() {

    $.fn.dataTable.ext.order.intl();

    var t = $('#poslanci').DataTable( {
      
      language: {
                url: "https://interaktivni.rozhlas.cz/tools/datatables/Czech.json" },
        order : [[ 6, "desc"]],
        data: data,
        columns: [
            { title: ""},
            { title: "Jméno"},
            { title: "Funkce" },
            { title: "Pohlaví" },
            { title: "Strana"},
            { title: "Znaky"},
            { title: "Slova"},
            { title: "Minuty"}
        ],
        responsive: true,
        columnDefs: [
            {   
                targets: 0,
                searchable: false,
                orderable: false,
            },
            {
                render: function (data, type, row) {
                    if (type == "sort" || type == 'type')
                        return data;
                    else
                        return row[0] + " " + data ;
                },
                targets: 1,
                responsivePriority: 1
            },
            {
                targets: 2,
            },
            {   
                targets: 3, 
                visible: false,
                searchable: false  
            },      
            {
                targets: 4,
                responsivePriority: 3
            },
            {   
                targets: 5,
                visible: false,
                searchable: false    
            },  
            {   
                render: function (data, type, row) {
                    if (type == "sort" || type == 'type')
                        return data;
                    else
                        return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;");
                },
                targets: 6,
                responsivePriority: 2,
                searchable: false    
            },  
            {   
                targets: 7,
                render: function (data, type, row) {
                    if (type == "sort" || type == 'type')
                        return data;
                    else
                        return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                },              
                responsivePriority: 4,  
                searchable: false    
            },               
        ],
     
    });    

   t.column(0).nodes().each( function (cell, i) {
            cell.innerHTML = "<b>" + (i+1) + "</b>";
        } );

});